package pens.ac.id.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pens.ac.id.dao.DaoBidang;
import pens.ac.id.dto.Row;
import pens.ac.id.model.Bidang;


@Service
public class ServiceBidang {

    public static class Row extends pens.ac.id.dto.Row{
        public Row() {
            type = "bidang";
        }
    }

	@Autowired
	private DaoBidang daoBidang;

	public DaoBidang getDao() {
		return daoBidang;
	}

	public void save(Bidang bidang) {
		daoBidang.save(bidang);
	}

	public boolean update(Bidang bidang) {
		Bidang oldBidang = daoBidang.findOne(bidang.getId());
		if(oldBidang != null){
			oldBidang.setNama(bidang.getNama());
			daoBidang.save(oldBidang);
			return true;
		}
		return false;
	}

	public List<Bidang> getAll() {
		return daoBidang.findAll();
	}

	public Bidang getById(Long bidangId) {
		return daoBidang.findOne(bidangId);
	}

	public void delete(Long bidangId) {
		daoBidang.delete(bidangId);
	}

	public List<pens.ac.id.dto.Row> getAllAsRow(){
        List<pens.ac.id.dto.Row> rows = new ArrayList<>();
        for(Bidang bidang : getAll()){
            Row row = new Row();
            row.id = bidang.getId();
            row.cols.add(bidang.getId()+"");
            row.cols.add(bidang.getNama());
            rows.add(row);
        }
        return rows;
    }

    public List<String> columns(){
        List<String> cols = new ArrayList<>();
        cols.add("ID");
        cols.add("Bidang");
        cols.add("Edit");
        cols.add("Delete");
        return cols;
    }
}
