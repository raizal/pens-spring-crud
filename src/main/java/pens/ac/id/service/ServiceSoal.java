package pens.ac.id.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pens.ac.id.dao.DaoSoal;
import pens.ac.id.model.Bidang;
import pens.ac.id.model.Soal;
import pens.ac.id.model.Tingkat;


@Service
public class ServiceSoal {

    @Autowired
	private DaoSoal daoSoal;
	
	@Autowired
	private ServiceBidang serviceBidang;
	
	@Autowired
	private ServiceTingkat serviceTingkat;
	
	
	public void save(Soal soal) throws Exception {
		Bidang bidang = serviceBidang.getById(soal.getBidang().getId());
		if(bidang==null){
			throw new Exception(String.format("bidang id %d tidak tidak ditemukan", soal.getBidang().getId()));
		}
		
		Tingkat tingkat = serviceTingkat.getById(soal.getTingkat().getId());
		
		if(tingkat==null){
			throw new Exception(String.format("tingkat id %d tidak tidak ditemukan", soal.getTingkat().getId()));
		}
		
		Soal newSoal = new Soal();
//		newSoal.setTingkatId(soal.getTingkatId());
//		newSoal.setBidangId(soal.getBidangId());
		newSoal.setBidang(bidang);
		newSoal.setTingkat(tingkat);
		newSoal.setTextSoal(soal.getTextSoal());
		daoSoal.save(newSoal);
	}

	public boolean update(Soal soal) throws Exception {
		Bidang bidang = serviceBidang.getById(soal.getBidang().getId());
		if(bidang==null){
			throw new Exception(String.format("bidang id %d tidak tidak ditemukan", soal.getBidang().getId()));
		}
		
		Tingkat tingkat = serviceTingkat.getById(soal.getTingkat().getId());
		
		if(tingkat==null){
			throw new Exception(String.format("tingkat id %d tidak tidak ditemukan", soal.getTingkat().getId()));
		}
		
		Soal newSoal = daoSoal.findOne(soal.getId());
		if(newSoal != null){
//			newSoal.setTingkatId(soal.getTingkatId());
//			newSoal.setBidangId(soal.getBidangId());
			newSoal.setBidang(bidang);
			newSoal.setTingkat(tingkat);
			newSoal.setTextSoal(soal.getTextSoal());
			daoSoal.save(newSoal);
			return true;
		}
		
		return false;
	}


	public List<Soal> getAll() {		
		return daoSoal.findAll();
	}


	public Soal getById(Long soalId) {
		return daoSoal.findOne(soalId);
	}
	
	public void delete(Long soalId) {
		daoSoal.delete(soalId);
	}

    public static class Row extends pens.ac.id.dto.Row{
        public Row() {
            type = "soal";
        }
    }

    public List<pens.ac.id.dto.Row> getAllAsRow(){
        List<pens.ac.id.dto.Row> rows = new ArrayList<>();
        for(Soal soal : getAll()){
            Row row = new Row();
            row.id  = soal.getId();
            row.cols.add(soal.getId()+"");
            row.cols.add(soal.getBidang().getNama());
            row.cols.add(soal.getTingkat().getNama());
            row.cols.add(soal.getTextSoal());
            rows.add(row);
        }
        return rows;
    }

    public List<String> columns(){
        List<String> cols = new ArrayList<>();
        cols.add("ID");
        cols.add("Bidang");
        cols.add("Tingkat");
        cols.add("Soal");
        cols.add("Edit");
        cols.add("Delete");
        return cols;
    }

}
