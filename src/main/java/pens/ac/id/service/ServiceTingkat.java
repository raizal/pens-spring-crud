package pens.ac.id.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import pens.ac.id.dao.DaoTingkat;
import pens.ac.id.model.Soal;
import pens.ac.id.model.Tingkat;


@Service
public class ServiceTingkat {

	@Autowired
	private DaoTingkat daoTingkat;
	

	public DaoTingkat getDao() {
		return daoTingkat;
	}

	public void save(Tingkat tingkat) {
		daoTingkat.save(tingkat);
	}

	public boolean update(Tingkat tingkat) {
		Tingkat oldTingkat = daoTingkat.findOne(tingkat.getId());
		if(oldTingkat != null){
			oldTingkat.setNama(tingkat.getNama());
			daoTingkat.save(oldTingkat);
			return true;
		}
		return false;
	}

	public List<Tingkat> getAll() {
		return daoTingkat.findAll();
	}

	public Tingkat getById(Long tingkatId) {
		return daoTingkat.findOne(tingkatId);
	}
	
	public void delete(Long tingkatId) {
		daoTingkat.delete(tingkatId);
	}

    public static class Row extends pens.ac.id.dto.Row{
        public Row() {
            type = "tingkat";
        }
    }

    public List<pens.ac.id.dto.Row> getAllAsRow(){
        List<pens.ac.id.dto.Row> rows = new ArrayList<>();
        for(Tingkat tingkat : getAll()){
            Row row = new Row();
            row.id = tingkat.getId();
            row.cols.add(tingkat.getId()+"");
            row.cols.add(tingkat.getNama());
            rows.add(row);
        }
        return rows;
    }

    public List<String> columns(){
        List<String> cols = new ArrayList<>();
        cols.add("ID");
        cols.add("Tingkat");
        cols.add("Edit");
        cols.add("Delete");
        return cols;
    }

}
