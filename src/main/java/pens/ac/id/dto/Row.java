package pens.ac.id.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raizal.pregnanta on 07/01/2017.
 */
public class Row {
    public long id=0;
    public String type="";
    public List<String> cols = new ArrayList<>();
}
