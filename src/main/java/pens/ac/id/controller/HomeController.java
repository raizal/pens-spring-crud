package pens.ac.id.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by raizal.pregnanta on 07/01/2017.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String index(){
        return "/dashboard";
    }

}
